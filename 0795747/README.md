# Course2023-2024

Resources and assignments for WebAI Course at KULeuven (2023-2024)

## Add your files

This is the normal process that you will have to follow in order to interact with the repository:

* Clone the latest Version:
* Make any changes to your folder in the repository according to the specific assignment
* Commit your changes into your local repository
* Push your changes to the online repository


